//
//  bga-Lift.swift
//  Bga-Lift
//
//  Created by iosakademija on 11/28/16.
//  Copyright © 2016 iosakademija. All rights reserved.
//

import UIKit

class bga_Lift: UIPageViewController, UIPageViewControllerDataSource {
    
    
    
    private(set) lazy var ordredViewControllers : [UIViewController] = {
        return [ self.newViewController("Activities"),
                 self.newViewController("Info")]
    }()
    
    private func newViewController(_ controller : String) -> UIViewController {
        
        return UIStoryboard(name: "bga-Lift", bundle: nil).instantiateViewController(withIdentifier: "\(controller)")
    }
}

typealias Internal = bga_Lift
extension Internal {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        
        if let firstViewController = ordredViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
    }
}

extension bga_Lift {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = ordredViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return ordredViewControllers.last
        }
        
        guard ordredViewControllers.count > previousIndex else {
            return nil
        }
        
        return ordredViewControllers[previousIndex]
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewCOntrollerIndex = ordredViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewCOntrollerIndex + 1
        let ordredVewControllerCount = ordredViewControllers.count
        
        guard ordredVewControllerCount != nextIndex else {
            return ordredViewControllers.first
        }
        
        guard ordredVewControllerCount > nextIndex else {
            return nil
        }
        
        return ordredViewControllers[nextIndex]
    }
    
}












